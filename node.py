# -*- coding: utf-8 -*-

# node.py
# copyright 2014 яша <yasha@xyzzy.be>

# This file is part of xx.
#
# xx is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# xx is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

import os
import uuid
import logging
import subprocess

log = logging.getLogger(__name__)

JUNK_DIR = "/tmp/xx/"

try:
    os.makedirs(JUNK_DIR)
except FileExistsError:
    log.info("junk dir %s already exists", JUNK_DIR)

class node(object):
    def __init__(self):
        self.sink = os.path.join(JUNK_DIR, str(uuid.uuid4()))
        self.sources = list()
        self.listeners = set()
        self.proc = subprocess.Popen(["/usr/bin/true"])
    @property
    def on(self):
        # double negation here to force a True or False rather than
        # return code
        return not not self.proc.poll()
    @property
    def off(self):
        return not self.proc.poll()
    def attach(self, n):
        if not n in self.sources:
            self.sources.append(n)
        n.listeners.add(self)
    def detach(self, n):
        while n in self.sources:
            self.sources.remove(n)
        n.listeners.discard(self)
    def spawn(self):
        try:
            os.mkfifo(self.sink)
        except OSError as e:
            log.info("couldn't spawn fifo: %s", e)
    def despawn(self):
        if self.on:
            self.stop()
        try:
            os.unlink(self.sink)
        except OSError as e:
            log.info("couldn't despawn fifo: %s", e)
    def respawn(self):
        self.despawn()
        self.spawn()
    def start(self):
        if self.off:
            self.proc = subprocess.Popen(["/usr/bin/true"] + self.sources)
    def stop(self):
        for s in self.sources:
            if s.on:
                s.stop()
        try:
            self.proc.terminate()
        except OSError as e:
            log.info("couldn't stop proc: %s", e)
    def restart(self):
        self.stop()
        self.start()
