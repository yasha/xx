# -*- coding: utf-8 -*-

# sox.py
# copyright 2014 яша <yasha@xyzzy.be>

# This file is part of xx.
#
# xx is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# xx is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

import node
import subprocess

class sox(node.node):
    def __init__(self):
        BaseClass.__init__(self)
        self.global_opts = ["--buffer", "0",
                            "--no-show-progress",
                            "-R"]
        self.format_opts = ["--type", "sox"]
        self.effects = []
    def start(self):
        if self.off:
            infiles = []
            for s in sources:
                infiles += self.format_opts + s.sink
            self.proc = subprocess.Popen(["/usr/bin/sox"] +
                                         self.global_opts +
                                         infiles + self.format_opts +
                                         [self.sink] + self.effects)
